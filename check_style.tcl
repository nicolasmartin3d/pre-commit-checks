#!/usr/bin/tclsh

proc sputs {msg} {
    puts "\[check_style\] $msg"
}

proc usage {} {
    global argv0
    puts "$argv0 \[-a\] \[-h\] \[file\]"
    exit 1
}

set files {}
set dry_run 0
for {set i 0} {$i < [llength $argv]} {incr i} {
    set arg [lindex $argv $i]
    if {$arg == "-h"} {
        [usage]
    } elseif {$arg == "-d"} {
        set dry_run 1
    } elseif {$arg == "-a"} {
        set k [catch {exec git ls-files . | grep -e {\.cpp} -e {\.h} -e {CMakeLists} -e {\.in} -e {\.tpp}} files]
        if {$k} {
            sputs "Error listing files : $files"
            exit 1
        }
    } else {
        if {[string index $arg 0] == "-"} {
            sputs "Error while parsing : $arg"
            exit 1
        }
        lappend files [lindex $argv $i]
    }
}

if {[llength $files] == 0} {
    set k [catch {exec git diff-index HEAD --diff-filter=ACMRTUXB --name-only} files]
    if {$k} {
        sputs "Error listing files : $files"
        exit 1
    }
}

sputs "Running check for :\n$files"

set config [file join [file dirname [file normalize $argv0]] "uncrustify.cfg"]
# sputs "config = $config"
set cmd "uncrustify -q -c $config --no-backup -l CPP "

set to_ignore {}
set k [catch {set ignore_fid [open [file join [exec git rev-parse --show-toplevel] ".checkignore"] r]} infos]
if {!$k} {
    set to_ignore [read $ignore_fid]
    close $ignore_fid
}

foreach f $files {
    if {[file isdirectory $f]} {continue}

    set ignore 0
    foreach i $to_ignore {
        if {[string match $i $f]} { 
            set ignore 1
        }
    }
    if {$ignore} {
        sputs "File $f ignored."
        continue
    }

    set fd [open $f r]
    set data [read  $fd]
    close $fd
    set line [lindex [split $data "\n"] 0]

    regexp {^[^.]*\.(.*)} $f -> ext

    #check the extension first !
    switch $ext {
        "cpp" -
        "c" -
        "hpp" -
        "h" -
        "h.in" -
        "tpp" {
            if {![string equal [string index $line 0] "/"]} {
                sputs "Fist line of $f is not a comment. Source files must begin with a comment (and copyright)."
                exit 1
            }
            if {!$dry_run} {
                set k [catch { eval exec $cmd $f } infos]
            }

            #uncrustify sometimes hang on a line .. must not stop the commit
            #if ${k} { puts "Error while running uncrustify : $infos" ; exit 1 }

            #if {[file exists ${f}.orig]} {
                #set fd [open "${f}.orig" r]
                #set new_data [read $fd]
                #close $fd

                #if {![string equal $data $new_data]} {
                    #puts "Style not validated ... Do you want to merge the change automatically ? \[y/N\]"
                    #set ans [gets $cin]
                    #if {[string equal $ans "y"]} {
                        #set fd [open $f w]
                        #puts $fd $new_data
                        #close $fd
                    #} else {
                        #puts "Changes are stored in file ${f}.orig, you can see the changes by using diff -u $f ${f}.orig"
                    #}
                #}
            #}
        }
    }
}


set k [catch {exec git --no-pager diff} diff]
if {$k} {
    sputs "Could not find differences using git diff"
    exit 1
}

if {![string equal $diff ""]} {
    sputs "At least one file has been automatically modified, check it using git difftool"
    exit 1
} else {
    sputs "Nothing to do! Yeah ;)"
    exit 0
}
