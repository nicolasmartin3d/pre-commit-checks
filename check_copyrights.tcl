#!/usr/bin/tclsh

proc sputs {msg} {
    puts "\[check_copyrights\] $msg"
}

proc usage {} {
    global argv0
    puts "$argv0 \[-a\] \[-i\] \[-h\] \[file\]"
    exit 1
}

set files {}
set interactive 0
set dry_run 0
for {set i 0} {$i < [llength $argv]} {incr i} {
    set arg [lindex $argv $i]
    if {$arg == "-h"} {
        [usage]
    } elseif {$arg == "-d"} {
        set dry_run 1
    } elseif {$arg == "-i"} {
        set interactive 1
    } elseif {$arg == "-a"} {
        set k [catch {exec git ls-files . | grep -e {\.cpp} -e {\.h} -e {CMakeLists} -e {\.in} -e {\.tpp}} files]
        if {$k} {
            sputs "Error listing files : $files"
            exit 1
        }
    } else {
        if {[string index $arg 0] == "-"} {
            sputs "Error while parsing : $arg"
            exit 1
        }
        lappend files [lindex $argv $i]
    }
}

if {[llength $files] == 0} {
    set k [catch {exec git diff-index HEAD --diff-filter=ACMRTUXB --name-only} files]
    if {$k} {
        sputs "Error listing files : $files"
        exit 1
    }
}

sputs "Running check for :\n$files"
if {$interactive} {
    sputs "Interactive mode, you will be asked what to do for each files"
} else {
    sputs "Non interactive mode, modifications will be made automatically"
}

set to_ignore {}
set k [catch {set ignore_fid [open [file join [exec git rev-parse --show-toplevel] ".checkignore"] r]} infos]
if {!$k} {
    set to_ignore [read $ignore_fid]
    close $ignore_fid
}

set k [catch {set cmake_fid [open [file join [exec git rev-parse --show-toplevel] "CMakeLists.txt"] r]} infos]
#set k [catch {set cmake_fid [open [file join [file dirname [file normalize $argv0]] "../CMakeLists.txt"] r]} infos]
if {$k} {
    sputs "No CMakeLists.txt file found"
    exit 1
}
set cmake_contents [read $cmake_fid]
close $cmake_fid

if {![regexp {PROJECT "([^\"]*)"} ${cmake_contents} -> project]} {
    sputs "No PROJECT variable found in CMakeLists.txt"
    exit 1
}
set project [string toupper $project]

if {![regexp [subst {${project}_AUTHOR "(\[^\"\]*)"}] ${cmake_contents} -> author]} {
    sputs "No ${project}_AUTHOR variable found in CMakeLists.txt"
    exit 1
}

if {![regexp [subst {${project}_EMAIL "(\[^\"\]*)"}] ${cmake_contents} -> email]} {
    sputs "No ${project}_EMAIL variable found in CMakeLists.txt"
    exit 1
}

if {![regexp [subst {${project}_YEARS "(\[^\"\]*)"}] ${cmake_contents} -> years]} {
    sputs "No ${project}_YEARS variable found in CMakeLists.txt"
    exit 1
}

if {![regexp [subst {${project}_PROJECT "(\[^\"\]*)"}] ${cmake_contents} -> software]} {
    sputs "No ${project}_PROJECT variable found in CMakeLists.txt"
    exit 1
}

if {![regexp [subst {${project}_DESCRIPTION "(\[^\"\]*)"}] ${cmake_contents} -> description]} {
    sputs "No ${project}_DESCRIPTION variable found in CMakeLists.txt"
    exit 1
}

#sputs "Found $software, $description, $author, $email, $years"

set copyright_software "$software"
set copyright_description "$description"
set copyright_holder "$author ($email)"
set copyright_year "$years"

set cpp_license_template {/*
 * %1$s - %2$s.
 * Copyright (C) %3$s %4$s
 *
 * This file is part of %1$s.
 *
 * %1$s is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * %1$s is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with %1$s.  If not, see <http://www.gnu.org/licenses/>.
 */}

set cpp_license_template_bsd3 {/*
 * License Agreement for %1$s
 *
 * Copyright (c) %3$s, %4$s
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the <organization> nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */}

set cmake_license_template {#
# %1$s - %2$s.
# Copyright (C) %3$s %4$s
#
# This file is part of %1$s.
#
# %1$s is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# %1$s is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with %1$s.  If not, see <http://www.gnu.org/licenses/>.
#}

set tty 0
set k [catch { set cin [open /dev/tty] } infos]
if {!$k} {set tty 1}

set cpp_license [format $cpp_license_template $copyright_software $copyright_description $copyright_year $copyright_holder]
set cpp_license_bsd3 [format $cpp_license_template_bsd3 $copyright_software $copyright_description $copyright_year $copyright_holder]
set cmake_license [format $cmake_license_template $copyright_software $copyright_description $copyright_year $copyright_holder]

set modified 0
foreach f $files {
    if {[file isdirectory $f]} {continue}

    set ignore 0
    foreach i $to_ignore {
        if {[string match $i $f]} { 
            set ignore 1
        }
    }
    if {$ignore} {
        sputs "File $f ignored."
        continue
    }

    set fd [open $f r]
    set data [read  $fd]
    close $fd

    set license ""
    set header ""
    set code ""
    regexp {^[^.]*\.(.*)} $f -> ext

    #check the extension first !
    switch $ext {
        "cpp" -
        "c" -
        "hpp" -
        "h" -
        "h.in" -
        "tpp" {
            regexp {^(/\*.*?\*/)(.*)$} $data -> header code
            if {[string match "*applications/*" $f] ||
                [string match "*test/*" $f] ||
                [string match "*examples/*" $f]} {
                set license $cpp_license_bsd3
            } else {
                set license $cpp_license
            }
        }
        "txt" {
            if {[string equal [file tail $f] "CMakeLists.txt"]} {
                regexp  {^(#.*?)(\n[^#].*)$} $data -> header code
                set license $cmake_license
            }
        }
        "cmake.in" {
            regexp  {^(#.*?)(\n[^#].*)$} $data -> header code
            set license $cmake_license
        }
    }

    if {[string equal $license ""]} { continue }
    
    if {![string equal $header $license]} {
        sputs "File $f has a non-valid license."
        #sputs "Found copyright : <$header>"
        #sputs "Expected : <$license>"
        if {$interactive && $tty} {
            sputs "Would you like to change it automatically ? \[y/N\]"
            set ans [gets $cin]
        } else {
            set ans "y"
        }
        if {[string equal $ans "y"]} {
            if {!$dry_run} {
                set data "${license}${code}"
                set fd [open $f w]
                puts -nonewline $fd $data
                close $fd
            }
            set modified 1
        }
    }
}
if {$modified} {
    sputs "At least one file has been automatically modified, check it using git difftool"
    exit 1
} else {
    sputs "Nothing to do! Yeah ;)"
    exit 0
}
